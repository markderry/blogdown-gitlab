---
title: "Intro & Set-up"
date: "2016-05-05T21:48:51-07:00"
---

# Intro
This is a quick crash course in getting setup with a programming environment and some of the basic skills to do data science type work.

## R
Install R from [Cran](https://cran.r-project.org/)

Install Rstudio from [Rstudio](https://www.rstudio.com/products/rstudio/download/)

## Git
Install Git from [Git](https://git-scm.com/downloads)

Get a gitlab account from [Gitlab](gitlab.com)

## Python
Install Python 3 from [Python Software Foundation](https://www.python.org/downloads/)

Install Rodeo from [Yhat](https://www.yhat.com/products/rodeo)

## Courses
Try R from [Codeschool](http://tryr.codeschool.com/)

Full data science track of courses from [Johns Hopkins University](https://www.coursera.org/specializations/jhu-data-science)

## Set up connection between gitlab and rstudio

## Build ssh key
1. Go to "Git Bash" just like cmd. Right click and "Run as Administrator".  
2. Type ```ssh-keygen``` Press enter.  
3. It will ask you to save the key to the specific directory.  
4. Press enter. It will prompt you to type password or enter without password.  
5. The public key will be created to the specific directory.  
6. Now go to the directory and open .ssh folder.  
7. You'll see a file id_rsa.pub. Open it on notepad. Copy all text from it.  
8. Go to https://gitlab.com/profile/keys .  
9. Paste here in the "key" textfield.  
10. Now click on the "Title" below. It will automatically get filled.  
11. Then click "Add key".  

# Build a Project connected to gitlab repo
1. Create a new project in Gitlab 
 1. [How to create a project in GitLab](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
 2. Copy ssh link to project
2. Create new project in rstudio 
 1. File
 2. New project
 3. Version control
 4. Git
 5. Paste previously copied link into "Repository URL"
 6. Click create project

