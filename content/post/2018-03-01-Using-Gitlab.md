---
title: "Using Gitlab"
date: 2018-03-01T13:09:13-06:00
---

**Using Gitlab** Git allows us to track all changes to our code and share those changes with others who may also be working on the same code who can in turn share changes they make back to the orginal code file. It also performs automated testing and deployment of the code to whatever platform it will ultimately reside on. 
